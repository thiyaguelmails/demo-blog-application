package com.ericsson.demo;

public class Calculator implements ICalculator {

	@Override
	public int sum(int a, int b) {
		return a + b;
	}


	@Override
	public int subtraction(int a, int b) {
		return a - b;
	}
	
	@Override
	public int multiplication(int a, int b) {
		return a * b;
	}

	@Override
	public int divison(int a, int b) throws Exception {		
		return a / b;
	}

	
	
}
