FROM openjdk:8-jdk-alpine

ARG deployable_artifact

COPY $deployable_artifact /root/app.jar

ENTRYPOINT ["java","-jar","/root/app.jar"]
